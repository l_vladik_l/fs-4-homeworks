package homework03;

import java.util.Arrays;
import java.util.Objects;

import libs.ScannerConstrained;

public class TaskPlanner {

    static String[] daysMap = new String[]{"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
    static String[] actionsMap = new String[]{"watch film", "go training", "learning", "walking", "singing", "playing", "working"};
    public static void print(String line) {
        System.out.println(line);
    }
    static void fillTasks(String[][] tasks) {
        for (int y = 0; y < tasks.length; y++) {
            tasks[y][0] = daysMap[y];
            tasks[y][1] = actionsMap[y];
        }

    }

    static boolean isValueExist(String[][] tasks, String task) {
        for (int y = 0; y < tasks.length; y++) {
            if (Objects.equals(tasks[y][0].toLowerCase(), task.toLowerCase()) || Objects.equals(tasks[y][1].toLowerCase(), task.toLowerCase())) {
                return true;
            }
        }
        return false;
    }
    static String findAction(String[][] tasks, String task) {
        String value = "";
        for (int y = 0; y < tasks.length; y++) {
            for (int x = 0; x < tasks[y].length; x++) {
                if (Objects.equals(tasks[y][x].toLowerCase(), task.toLowerCase())) {
                    value = tasks[y][1];
                }
            }
        }
        return value;
    }
    public static void main(String[] args) {
        String[][] tasks = new String[7][2];
        boolean isAsking = true;
        fillTasks(tasks);
        ScannerConstrained scanner = new ScannerConstrained();

        while (isAsking) {

            print("Please, input the day of the week:");

            String line = scanner.nextLine().trim();

            switch (line) {
                case ("exit"):
                    isAsking = false;
                    break;

                default:
                    boolean isExist = isValueExist(tasks, line);
                    if (!isExist) {
                        print("Sorry, I don't understand you, please try again.");
                        continue;
                    }
                    print(String.format("Your tasks for %s: %s", line, findAction(tasks, line)));
                    break;
            }
        }

    }
}
