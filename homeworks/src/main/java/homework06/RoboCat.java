package homework06;

public class RoboCat extends Pat implements Foulable {

    public RoboCat(AnimalSpecies species, String nickname, Integer age, Integer trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(species);
    }

    @Override
    public String respond() {
        return "I can respond as you want Im a robot";
    }

    @Override
    public String foul() {
        return "I cant foul";
    }

    public static void main(String[] args) {
        RoboCat roboCat = new RoboCat(AnimalSpecies.CAT, "RoboCat", 1000, 100, new String[]{});
        System.out.println(roboCat.eat());
        System.out.println(roboCat.getHabits());
        System.out.println(roboCat.foul());

    }

}
