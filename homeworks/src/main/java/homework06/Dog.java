package homework06;

public class Dog extends Pat implements Foulable {

    public Dog(AnimalSpecies species, String nickname, Integer age, Integer trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(species);
    }

    @Override
    public String respond() {
        return "Gav";
    }

    @Override
    public String foul() {
        return "I can eat sofa and food on kitchen";
    }

    public static void main(String[] args) {
        Dog dog = new Dog(AnimalSpecies.DOG, "Charlie", 2, 100, new String[]{"walking", "studying"});
        System.out.println(dog.eat());
        System.out.println(dog.getHabits());
        System.out.println(dog.foul());

    }

}
