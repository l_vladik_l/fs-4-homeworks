package homework06;

import homework02.Shooting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Family implements HumanCreator {

    private final Human mother;
    private final Human father;
    private final Pat pat;
    private Human[] children;

    public Family(Human mother, Human father, Pat pat, Human[] children) {
        this.mother = mother;
        this.father = father;
        this.pat = pat;
        this.children = children;
    }

    static Family of(Human mother, Human father, Pat pat, Human[] children) {
        return new Family(mother, father, pat, children);
    }
    static Family makeFamily(Human mother, Human father, Pat pat) {
        return new Family(mother, father, pat, new Human[]{});
    }
    void addChild(Human child) {
        Human[] copy = new Human[children.length + 1];
        System.arraycopy(children, 0, copy, 0, children.length);
        copy[children.length] = child;
        this.children = copy;
    }
    int countFamily() {
        if (children.length == 0) return 2;
        return this.children.length + 2;
    }
    void removeChild(int idx) {
        if (this.children.length == 0) throw new IllegalStateException("no children provided");
        if (idx < 0) throw new IllegalStateException("index must be positive");
        if (idx >= this.children.length) throw new IllegalStateException("index must be less than length");

        Human[] newArray = new Human[this.children.length - 1];
        System.arraycopy(this.children, 0, newArray, 0, idx);
        System.arraycopy(this.children, idx + 1, newArray, idx, newArray.length - idx);
        this.children = newArray;
    }

    void greetPat() {
        Shooting.print(String.format("Hi, %d", pat.getTrickLevel()));
    }
    void describePat() {
        Integer trickle = pat.getTrickLevel();
        String describeTrick = trickle >= 50 ? "very tricky" : "not very tricky";
        Shooting.print(String.format("I got %s, he is %d, he is very %s", trickle, pat.getPatAge(), describeTrick));
    }

    private String[] addNames(String[] arr, String newValue) {
        String[] newArr = new String[arr.length + 1];
        System.arraycopy(arr, 0, newArr, 0, arr.length);
        newArr[arr.length] = newValue;
        return newArr;
    }
    @Override
    public String toString() {
        boolean isChildrenExist = this.children.length != 0;
        String[] names = new String[]{};
        if (isChildrenExist) {
            for (Human child : this.children) {
                names = addNames(names, child.getName());
            }
        }
        String[] humansNames = isChildrenExist ? names : new String[]{};

        return String.format("motherName=%s, motherSurname=%s, motherYear=%d, motherIq=%d,fatherName=%s, fatherSurname=%s, fatherYear=%d, fatherIq=%d, pat=%s, habits=%s, children=%s",
                this.mother.getName(), this.mother.getSurname(), this.mother.getYear(), this.mother.getIqLevel(),
                this.father.getName(), this.father.getSurname(), this.father.getYear(), this.father.getIqLevel(),
                this.pat.toString(), this.pat.getHabits(), Arrays.toString(humansNames));
    }

    @Override
    public Human bornChild(String name, Integer year) {
        Man man = new Man(name, this.father.getSurname(), (this.father.getIqLevel() + this.mother.getIqLevel()) / 2, year);
        Woman woman = new Woman(name, this.father.getSurname(), (this.father.getIqLevel() + this.mother.getIqLevel()) / 2, year);
        ArrayList<Human> list = new ArrayList<>();
        list.add(man);
        list.add(woman);
        return list.get(new Random().nextInt(list.size()));
    }

    public static void main(String[] args) {
        Woman mother = new Woman("motherName", "motherSurname", 100, 1979);
        Man father = new Man("fatherName", "fatherSurnmae", 100, 1963);
        mother.makeUp();
        father.repairCar();
        Pat pat = new Fish(AnimalSpecies.FISH, "Bulka", 100, 0, new String[]{"swim"});
        Human baby = Human.baseHumanInfo("babyName", "babySurname", 2021);
        Human baby2 = Human.baseHumanInfo("baby2Name", "baby2Surname", 2022);

        Family family = Family.makeFamily(mother, father, pat);
        Human bornBaby = family.bornChild("baby", 2022);
        family.addChild(bornBaby);
        System.out.println(family);
        System.out.println(bornBaby);
        family.addChild(baby);
        family.addChild(baby2);
        family.addChild(baby2);
        family.removeChild(0);
        family.removeChild(1);
        AnimalSpecies bird = AnimalSpecies.BIRD;

        System.out.println(bird.hasFur());
        System.out.println(bird.numberOfLegs());

        System.out.println(family.countFamily());

        System.out.println(Arrays.toString(family.children));

    }

}
