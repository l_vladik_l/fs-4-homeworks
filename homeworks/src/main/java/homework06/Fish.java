package homework06;

public class Fish extends Pat implements Foulable {

    public Fish(AnimalSpecies species, String nickname, Integer age, Integer trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(species);
    }

    @Override
    public String respond() {
        return "I`m a fish I cant provide any sound";
    }

    @Override
    public String foul() {
        return "I cant do any foul I just can swim";
    }

    public static void main(String[] args) {
        Fish fish = new Fish(AnimalSpecies.FISH, "lola", 12, 12, new String[]{});
        System.out.println(fish.eat());
        System.out.println(fish.getHabits());
        System.out.println(fish.foul());

    }

}
