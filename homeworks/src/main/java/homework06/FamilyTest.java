package homework06;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FamilyTest {

    @Test
    public void testAddChild() {
        Woman mother = new Woman("motherName", "motherSurname", 100, 1979);
        Man father = new Man("fatherName", "fatherSurnmae", 100, 1963);
        Pat pat = new Fish(AnimalSpecies.FISH, "Bulka", 100, 0, new String[]{"swim"});
        Family family = Family.makeFamily(mother, father, pat);
        Human baby = Human.baseHumanInfo("babyName", "babySurname", 2021);
        family.addChild(baby);
        assertEquals(3, family.countFamily());
    }
    @Test
    public void testDeleteChild() {
        Woman mother = new Woman("motherName", "motherSurname", 100, 1979);
        Man father = new Man("fatherName", "fatherSurnmae", 100, 1963);
        Pat pat = new Fish(AnimalSpecies.FISH, "Bulka", 100, 0, new String[]{"swim"});
        Family family = Family.makeFamily(mother, father, pat);
        Human baby = Human.baseHumanInfo("babyName", "babySurname", 2021);
        Human baby2 = Human.baseHumanInfo("babyName", "babySurname", 2021);
        family.addChild(baby);
        family.addChild(baby2);
        assertEquals(4, family.countFamily());

        family.removeChild(0);

        assertEquals(3, family.countFamily());
    }
    @Test
    public void testToString() {
        Woman mother = new Woman("motherName", "motherSurname", 100, 1979);
        assertEquals(true, mother.toString() instanceof String);
    }
}
