package homework06;

public class DomesticCat extends Pat implements Foulable {

    public DomesticCat(AnimalSpecies species, String nickname, Integer age, Integer trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(species);
    }

    @Override
    public String respond() {
        return "Meow";
    }

    @Override
    public String foul() {
        return "puuur and pee everywhere";
    }

    public static void main(String[] args) {
        DomesticCat cat = new DomesticCat(AnimalSpecies.CAT, "Julka", 12, 100, new String[]{"sleep", "eat"});
        System.out.println(cat.eat());
        System.out.println(cat.getHabits());
        System.out.println(cat.foul());

    }

}
