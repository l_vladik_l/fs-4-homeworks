package homework06;

public interface HumanCreator {

    Human bornChild(String name, Integer year);
}
