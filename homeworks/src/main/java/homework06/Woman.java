package homework06;

public final class Woman extends Human {

    public Woman(String name, String surname, Integer iqLevel, Integer year) {
        super(name, surname, iqLevel, year);
    }
    @Override
    public void greetPat() {
        System.out.println("hello y little girl");
    }
    public void makeUp() {
        System.out.println("I can makeup");
    }

}
