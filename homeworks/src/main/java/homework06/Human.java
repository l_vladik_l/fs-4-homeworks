package homework06;

public class Human {

    private final String name;
    private final String surname;
    private final Integer iqLevel;
    private final Integer year;

    public Human(String name, String surname, Integer iqLevel, Integer year) {
        this.name = name;
        this.surname = surname;
        this.iqLevel = iqLevel;
        this.year = year;

    }

    static Human of(String name, String surname, Integer iqLevel, Integer year) {
        return new Human(name, surname, iqLevel, year);
    }
    static Human baseHumanInfo(String name, String surname, Integer year) {
        return new Human(name, surname, null, year);
    }

    public void greetPat() {
        System.out.println("hello y little boy");
    }

    public String getName() {
        return this.name;
    }
    public String getSurname() {
        return this.surname;
    }
    public Integer getYear() {
        return this.year;
    }
    public Integer getIqLevel() {
        return this.iqLevel;
    }

    @Override
    public String toString() {
        return String.format("name=%s, surname=%s, year=%d, iq=%d", this.name, this.surname, this.year, this.iqLevel);
    }

}
