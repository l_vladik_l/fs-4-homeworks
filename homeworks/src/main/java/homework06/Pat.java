package homework06;

import java.util.Arrays;

public abstract class Pat {

    private AnimalSpecies species;
    private final String nickname;
    private final Integer age;
    private final Integer trickLevel;
    private final String[] habits;

    public Pat(String nickname, Integer age, Integer trickLevel, String[] habits) {

        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pat() {

        this.nickname = "";
        this.age = 0;
        this.trickLevel = 0;
        this.habits = new String[]{};
    }
    public Pat(String nickname) {
        this.nickname = nickname;
        this.age = 0;
        this.trickLevel = 0;
        this.habits = new String[]{};
    }

    protected void setSpecies(AnimalSpecies species) {
        this.species = species;
    }

    @Override
    public String toString() {
        return String.format("%s {nickname=%s, age=%d, trickLevel=%d, habits=%s", this.species, this.nickname, this.age, this.trickLevel, Arrays.toString(this.habits));
    }

    public String eat() {
        return "Im eating";
    }
    public abstract String respond();

    public Integer getTrickLevel() {
        return this.trickLevel;
    }
    public Integer getPatAge() {
        return this.age;
    }
    public String getHabits() {
        return Arrays.toString(this.habits);
    }

}
