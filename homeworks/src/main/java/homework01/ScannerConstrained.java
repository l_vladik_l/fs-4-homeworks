package homework01;

import java.io.InputStream;
import java.util.Scanner;

public class ScannerConstrained {
    InputStream in = System.in;
    private final Scanner scanner = new Scanner(in);

    public String nextLine() {
        return scanner.nextLine();
    }
}
