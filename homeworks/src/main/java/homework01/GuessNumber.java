package homework01;

import java.util.Arrays;

public class GuessNumber {
    public static void printS(String line) {
        System.out.printf("%s", line);
    }
    public static void printN(Integer number) {
        System.out.printf("%d", number);
    }
    public static int random(int min, int max) {
        int range = max - min + 1;
        double rand = Math.random() * range;
        return (int) (rand + min);
    }

    public static String arrayToString(int[] array) {
        return Arrays.toString(array);
    }
    public static int[] addItem(int arr[], int newValue) {
        int newArr[] = new int[arr.length + 1];
        for (int i = 0; i < arr.length; i++)
            newArr[i] = arr[i];

        newArr[arr.length] = newValue;
        return newArr;
    }
    public static void main(String[] args) {
        int[] arr = {};

        ScannerConstrained scanner = new ScannerConstrained();
        int guessInt = random(0, 100);
        int inputInt = 0;

        printS("Let the game begin!\n");
        printS("Tell me please your name: ");
        String name = scanner.nextLine();
        while (guessInt != inputInt) {
            printS("Tell me please your number: ");
            String number = scanner.nextLine();
            try {
                int i = Integer.parseInt(number);
                inputInt = i;
                arr = addItem(arr, i);

                if (inputInt > guessInt) {
                    printS("Your number is too big. Please, try again.");
                }
                if (inputInt < guessInt) {
                    printS("Your number is too small. Please, try again.");
                }
            } catch (NumberFormatException ex) {
                printS("Your message cant be parsed to a number\n");
            }
        }
        Arrays.sort(arr);
        printS(arrayToString(arr));
        System.out.printf(" Congratulations, %s !", name);

    }
}
