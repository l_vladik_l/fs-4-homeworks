package app.model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class WorkPlaceForCustomer {
        private Long customer_id;
        private Long employer_id;

        @Override
        public String toString() {
                return "WorkPlaceForCustomer{" +
                        ", customerId='" + customer_id + '\'' +
                        ", employer_id='" + employer_id + '\'' +
                        '}';
        }
}
