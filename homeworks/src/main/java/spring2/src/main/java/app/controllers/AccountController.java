package app.controllers;

import app.entity.Account;
import app.entity.Currency;
import app.entity.Customer;
import app.model.AccountToReplenish;
import app.model.AccountToWithdraw;
import app.model.TransferFromTo;
import app.service.AccountService;
import app.service.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

@RestController
@RequestMapping("accounts")
@AllArgsConstructor
public class AccountController {

    private final AccountService accountService;
    private final CustomerService customerService;

    @GetMapping("generate")
    public void generateAccounts() {
        List<Long> customersId = customerService.
                findAll()
                .stream()
                .map(Customer::getId).toList();

        Optional
                .of(customersId)
                .filter(x -> !x.isEmpty())
                .orElseThrow(() -> new RuntimeException("Customer should be generated first"));

        Currency[] currency = Currency.values();

        customersId
                .forEach(id -> IntStream.range(0, 4)
                        .forEach(i -> accountService.saveAccountForCustomer(
                                new Account(currency[i], (double) ThreadLocalRandom.current().nextInt(1, 10000), new Customer()), id))

                );

    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<String> deleteAccount(@PathVariable("id") Long id) {
        return accountService.deleteById(id) ?
                ResponseEntity.ok("Account was deleted") :
                ResponseEntity.status(HttpStatus.NOT_FOUND).body("Account was not found");
    }
    @GetMapping("{id}")
    public Optional<Account> getOne(@PathVariable("id") Long id) {
        return accountService.getOne(id);
    }

    @PutMapping("top")
    public ResponseEntity<String> replenishAccount(@RequestBody AccountToReplenish ator) {
        return accountService.findByAccountNumber(ator.getNumber()).
                map(a -> {
                    accountService.accountToReplenish(a, ator.getBalance());
                    accountService.save(a);
                    return ResponseEntity.ok("Account have been replenished");
                })
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).body("Account not found"));
    }
    @PutMapping("withdraw")
    public ResponseEntity<String> replenishAccount(@RequestBody AccountToWithdraw atow) {
        return accountService.findByAccountNumber(atow.getNumber()).
                map(a -> accountService.accountToWithdraw(a, atow.getBalance()) ? ResponseEntity.ok("Account have been withdraw") : ResponseEntity.ok("Not enough money on balance"))
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).body("Account not found"));
    }

    @PutMapping("transfer")
    public ResponseEntity<String> replenishAccount(@RequestBody TransferFromTo fromTo) {
        Optional<Account> from = accountService.findByAccountNumber(fromTo.getFrom());
        Optional<Account> to = accountService.findByAccountNumber(fromTo.getTo());

        return from
                .flatMap(f -> to
                        .map(t -> accountService.transferFromTo(f, t, fromTo.getBalance()) ?
                                ResponseEntity.ok("Transfer was successful") :
                                ResponseEntity.ok("Not enough balace")))
                .orElseGet(() -> ResponseEntity
                        .status(HttpStatus.NOT_FOUND)
                        .body("'from' or 'to' account not found"));
    }
}
