package app.entity;

import app.model.AbstractEntity;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "customers")
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class Customer extends AbstractEntity  {
    private String name;
    private String email;
    private Integer age;

    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Account> accounts;

    public void addEmployer(Employer employer) {
        employers.add(employer);
        employer.getCustomers().add(this);
    }


    @ManyToMany(fetch = FetchType.EAGER)
//    @JoinTable(
//            name = "customer_workplace",
//            joinColumns = {
//                    @JoinColumn(name = "customer_id", referencedColumnName = "c_id")
//            },
//            inverseJoinColumns = {
//                    @JoinColumn(name = "employer_id", referencedColumnName = "e_id")
//            }
//    )
    private List<Employer> employers = new ArrayList<>();

    public Customer(String name, String email, Integer age) {
        this.name = name;
        this.email = email;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + getId() +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", age=" + age +
                '}';
    }
}
