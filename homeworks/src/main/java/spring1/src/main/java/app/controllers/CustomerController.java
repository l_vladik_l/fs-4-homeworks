package app.controllers;

import app.entity.Account;
import app.entity.Customer;
import app.model.CustomerToCreate;
import app.model.CustomerToUpdate;
import app.service.AccountService;
import app.service.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("customers")
@AllArgsConstructor
public class CustomerController {

    private final CustomerService customerService;
    private final AccountService accountService;

    @GetMapping("generate")
    public ResponseEntity<String> generateCustomers() {
        List.of(
                new Customer("Vlad", "vladikvladvita@gmail.com", 24),
                new Customer("Vitalina", "poradavita@gmail.com", 24),
                new Customer("Ruslan", "ystymenko@gmail.com", 25),
                new Customer("Nasty", "komarova@gmail.com", 22)
        ).forEach(customerService::save);
        return ResponseEntity.ok().body("customer have been generated");
    }

    @GetMapping("{id}")
    public ResponseEntity<Customer> getOne(@PathVariable("id") Long id) {
        return customerService.getOne(id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
    @GetMapping("all")
    public ResponseEntity<List<Customer>> getOne() {
        return ResponseEntity.ok().body(customerService.findAll());
    }
    @PostMapping("create")
    public ResponseEntity<Customer> createCustomer(@RequestBody CustomerToCreate customer) {
        Customer customer1 = new Customer();
        customer1.setAge(customer.getAge());
        customer1.setName(customer.getName());
        customer1.setEmail(customer.getEmail());
        return ResponseEntity.ok().body(customerService.save(customer1));
    }
    @PutMapping("{id}")
    public ResponseEntity<?> updateCustomer(@PathVariable Long id, @RequestBody CustomerToUpdate customer) {
        Optional<Customer> found = customerService.getOne(id);

        if (found.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        Customer customer1 = found.get();

        Integer age = customer.getAge();
        if (age != null) customer1.setAge(age);
        String email = customer.getEmail();
        if (email != null) customer1.setEmail(email);
        String name = customer.getName();
        if (name != null) customer1.setName(name);

        return ResponseEntity.ok().body(customerService.save(customer1));
    }

    @DeleteMapping("/remove/{id}")
    public ResponseEntity<?> removeCustomer(@PathVariable Long id) {
        return customerService.deleteById(id) ?
                ResponseEntity.noContent().build() :
                ResponseEntity.notFound().build();

    }

    @PostMapping("{id}/account/create")
    public ResponseEntity<Account> createAccountForCustomer(@PathVariable Long id, @RequestBody Account account) {
        Optional<Customer> found = customerService.getOne(id);

        if (found.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

        Account account1 = accountService.saveAccountForCustomer(account, id);

        return ResponseEntity.ok().body(account1);

    }
}
