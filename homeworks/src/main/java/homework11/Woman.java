package homework11;

public final class Woman extends Human {

    public Woman(String name, String surname, Integer iqLevel, String birthYear, String birthMonth, String birthDay) {
        super(name, surname, iqLevel, birthYear, birthMonth, birthDay);
    }
    @Override
    public void greetPat() {
        System.out.println("hello y little girl");
    }
    public void makeUp() {
        System.out.println("I can makeup");
    }

}
