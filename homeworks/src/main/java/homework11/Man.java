package homework11;

public final class Man extends Human {

    public Man(String name, String surname, Integer iqLevel, String birthYear, String birthMonth, String birthDay) {
        super(name, surname, iqLevel, birthYear, birthMonth, birthDay);
    }
    @Override
    public void greetPat() {
        System.out.println("hello y little boy");
    }
    public void repairCar() {
        System.out.println("I can repair car");
    }
}
