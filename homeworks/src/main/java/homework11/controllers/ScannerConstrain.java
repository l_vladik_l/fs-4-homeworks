package homework11.controllers;

import java.util.Scanner;

public class ScannerConstrain {
    private final Scanner sc = new Scanner(System.in);

    public String nextLine() {
        return sc.nextLine();
    }
}
