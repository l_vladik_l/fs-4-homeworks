package homework11;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class Human {

    private final String name;
    private final String surname;
    private final Integer iqLevel;
    private final String birthYear;
    private final String birthMonth;
    private final String birthDay;

    public Human(String name, String surname, Integer iqLevel, String birthYear, String birthMonth, String birthDay) {
        this.name = name;
        this.surname = surname;
        this.iqLevel = iqLevel;
        this.birthYear = birthYear;
        this.birthMonth = birthMonth;
        this.birthDay = birthDay;

    }

    static Human of(String name, String surname, Integer iqLevel, String birthYear, String birthMonth, String birthDay) {
        return new Human(name, surname, iqLevel, birthYear, birthMonth, birthDay);
    }
    static Human baseHumanInfo(String name, String surname, String birthYear, String birthMonth, String birthDay) {
        return new Human(name, surname, null, birthYear, birthMonth, birthDay);
    }

    public void greetPat() {
        System.out.println("hello y little boy");
    }

    public String getName() {
        return this.name;
    }
    public String getSurname() {
        return this.surname;
    }
    public int getYear() {
        return Integer.parseInt(this.birthYear);
    }

    public String getFormatterBirthday() {
        return String.format("%s/%s/%s", this.birthYear, this.birthMonth, this.birthDay);
    }
    public Integer getIqLevel() {
        return this.iqLevel;
    }

    public String prettyFormat() {
        return String.format("{ name=%s, surname='%s', iqLevel=%d } \n",
                this.name, this.surname, this.iqLevel);
    }

    @Override
    public String toString() {
        return String.format("name=%s, surname=%s, year=%d, iq=%d", this.name, this.surname, this.getYear(), this.iqLevel);
    }

}
