package homework11;

import homework11.controllers.FamilyController;
import homework11.controllers.ScannerConstrain;
import homework11.enums.AnimalSpecies;
import homework11.services.FamilyService;

import java.util.HashSet;
import java.util.List;

public class MyFamily {
    public static void main(String[] args) {

        Human mother = new Woman("motherName", "motherSurname", 100, "1979", "6", "10");
        Human father = new Man("fatherName", "fatherSurnmae", 100, "1979", "6", "10");

        HashSet<String> habits = new HashSet<>();
        habits.add("swim");
        Pat fish = new Fish(AnimalSpecies.FISH, "Bulka", 100, 0, habits);
        Pat roboCat = new RoboCat(AnimalSpecies.ROBOCAT, "ROBOCAT", 100, 0, habits);
        Human baby = Human.baseHumanInfo("babyName", "babySurname", "2021", "3", "5");
        Human baby2 = Human.baseHumanInfo("baby2Name", "baby2Surname", "2022", "1", "5");

        HashSet<Pat> pats = new HashSet<>();
        pats.add(roboCat);
        pats.add(fish);

        Family f1 = new Family(1, mother, father) {
        };
        Family f2 = new Family(2, mother, father) {
        };
        f1.addPat(fish);
        f2.addPat(roboCat);

        String text = """
                - 1. Fill several family test data.
                - 2. Depicted the list of families.
                - 3. Display a list of families where the number of people is greater than the specified number.
                - 4. Display a list of families where the number of people is less than the specified number query the number of people of interest
                - 5. Count the number of families where the number of members is equal to
                - 6. Создать новую семью //Create new family
                  - ask mother name
                  - ask father lastName
                  - ask mother year birthday
                  - ask mother month birthday
                  - ask mother day birthday
                  - ask mother iq
                  - ask father name
                  - ask father lastName
                  - ask father year birthday
                  - ask father month birthday
                  - ask father day birthday
                  - ask father iq
                - 7. Delete a family by its index in the general list
                - 8. To edit a family by the family index in the general list\s
                  - 1. Bord a child
                    - запросить порядковый номер семьи (ID)
                    - запросить необходимые данные (какое имя дать мальчику, какое девочке)
                  - 2. Adopt child
                    - запросить порядковый номер семьи (ID)
                    - запросить необходимые данные (ФИО, год рождения, интеллект)
                  - 3. Вернуться в главное меню \s
                - 9. Удалить всех детей старше возраста (во всех семьях удаляются дети старше указанного возраста - будем считать, что они выросли)
                  - запросить интересующий возраст
                """;

        FamilyController familyController = new FamilyController(new FamilyService());

        System.out.println(text);
        ScannerConstrain scanner = new ScannerConstrain();

        boolean isAsking = true;
        while (isAsking) {
            System.out.println("Tell me please number \n");
            String line = scanner.nextLine();
            switch (line) {
                case "1":
                    familyController.saveFamily(f1);
                    familyController.saveFamily(f2);
                    break;
                case "2":
                    List<Family> allFamilies = familyController.getAllFamilies();
                    System.out.println(allFamilies);
                    break;
                case "3":
                    String familiesBiggerThan = familyController.getFamiliesBiggerThan(5);
                    System.out.println(familiesBiggerThan);
                    break;
                case "4":
                    String familiesLessThan = familyController.getFamiliesLessThan(2);
                    System.out.println(familiesLessThan);
                    break;
                case "5":
                    int countFamilies = familyController.countFamiliesWithMemberNumber(5);
                    System.out.println(countFamilies);
                    break;
                case "7":
                    familyController.deleteFamilyByIndex(0);
                    break;

                //...rest
                case "10":
                    isAsking = false;
                    break;

            }
        }

    }
}
