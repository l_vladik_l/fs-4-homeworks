package homework11.interfaces;

public interface Identifable {
    int id(int uuid);
}
