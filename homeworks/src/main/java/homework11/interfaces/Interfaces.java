package homework11.interfaces;

import homework11.Human;

public class Interfaces {
    public interface HumanCreator {
        Human bornChild(String name, String birthYear, String birthMonth, String birthDay);
    }

    public interface Foulable {
        String foul();
    }

}
