package libs;

import java.util.Scanner;

public class ScannerConstrained {
    private final Scanner scanner = new Scanner(System.in);
    public String nextLine() {
        return scanner.nextLine();
    }
}
