package homework08.interfaces;

public interface Identifable {
    int id(int uuid);
}
