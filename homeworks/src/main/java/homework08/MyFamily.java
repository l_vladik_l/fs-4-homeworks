package homework08;

import homework08.controllers.FamilyController;
import homework08.enums.AnimalSpecies;
import homework08.services.FamilyService;

import java.util.HashSet;
import java.util.List;

public class MyFamily {
    public static void main(String[] args) {
        Woman mother = new Woman("motherName", "motherSurname", 100, 1979);
        Man father = new Man("fatherName", "fatherSurnmae", 100, 1963);

        HashSet<String> habits = new HashSet<>();
        habits.add("swim");
        Pat fish = new Fish(AnimalSpecies.FISH, "Bulka", 100, 0, habits);
        Pat roboCat = new RoboCat(AnimalSpecies.ROBOCAT, "ROBOCAT", 100, 0, habits);
        Human baby = Human.baseHumanInfo("babyName", "babySurname", 2021);
        Human baby2 = Human.baseHumanInfo("baby2Name", "baby2Surname", 2022);

        HashSet<Pat> pats = new HashSet<>();
        pats.add(roboCat);
        pats.add(fish);

        Family family = new Family(1, mother, father) {

        };
//        Human bornBaby = family.bornChild("baby", 2022);
//        family.addChild(bornBaby);
//        family.addChild(baby);
//        family.addChild(baby2);
//        family.addChild(baby2);
//        family.removeChild(0);
//        family.removeChild(1);
        AnimalSpecies bird = AnimalSpecies.BIRD;

        FamilyController familyController = new FamilyController(new FamilyService());
        familyController.saveFamily(family);
        familyController.createNewFamily(1, mother, father);

        familyController.addPet(0, fish);
        Family family1 = familyController.adoptChild(family, baby);
        List<Family> allFamilies = familyController.getAllFamilies();
        int i = familyController.countFamiliesWithMemberNumber(2);
        familyController.deleteAllChildrenOlderThen(1);
        System.out.println(i);
        System.out.println(family1);
    }
}
