package homework09;

import homework09.enums.AnimalSpecies;
import homework09.interfaces.Interfaces.Foulable;

import java.util.HashSet;

public class Fish extends Pat implements Foulable {

    public Fish(AnimalSpecies species, String nickname, Integer age, Integer trickLevel, HashSet<String> habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(species);
    }

    @Override
    public String respond() {
        return "I`m a fish I cant provide any sound";
    }

    @Override
    public String foul() {
        return "I cant do any foul I just can swim";
    }

}
