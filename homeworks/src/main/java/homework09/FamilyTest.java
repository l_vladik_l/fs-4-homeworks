package homework09;

import homework09.enums.AnimalSpecies;
import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FamilyTest {

    @Test
    public void testAddChild() {
        Woman mother = new Woman("motherName", "motherSurname", 100, 1979);
        Man father = new Man("fatherName", "fatherSurnmae", 100, 1963);
        HashSet<String> habits = new HashSet<>();
        habits.add("swim");
        Pat fish = new Fish(AnimalSpecies.FISH, "Bulka", 100, 0, habits);
        HashSet<Pat> pats = new HashSet<>();
        pats.add(fish);
        Family family = new Family(1, mother, father, pats) {
        };
        Human baby = Human.baseHumanInfo("babyName", "babySurname", 2021);
        family.addChild(baby);
        assertEquals(3, family.countFamily());
    }
    @Test
    public void testDeleteChild() {
        Woman mother = new Woman("motherName", "motherSurname", 100, 1979);
        Man father = new Man("fatherName", "fatherSurnmae", 100, 1963);
        HashSet<String> habits = new HashSet<>();
        habits.add("swim");
        Pat fish = new Fish(AnimalSpecies.FISH, "Bulka", 100, 0, habits);
        HashSet<Pat> pats = new HashSet<>();
        pats.add(fish);
        Family family = new Family(1, mother, father, pats) {
        };

        Human baby = Human.baseHumanInfo("babyName", "babySurname", 2021);
        Human baby2 = Human.baseHumanInfo("babyName", "babySurname", 2021);
        family.addChild(baby);
        family.addChild(baby2);
        assertEquals(4, family.countFamily());

        family.removeChild(0);

        assertEquals(3, family.countFamily());
    }
    @Test
    public void testToString() {
        Woman mother = new Woman("motherName", "motherSurname", 100, 1979);
        assertEquals(true, mother.toString() instanceof String);
    }
}
