package homework09;

import homework09.enums.AnimalSpecies;
import homework09.interfaces.Interfaces.Foulable;

import java.util.HashSet;

public class Dog extends Pat implements Foulable {

    public Dog(AnimalSpecies species, String nickname, Integer age, Integer trickLevel, HashSet<String> habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(species);
    }

    @Override
    public String respond() {
        return "Gav";
    }

    @Override
    public String foul() {
        return "I can eat sofa and food on kitchen";
    }

}
