package homework09.enums;

public enum AnimalSpecies {
    CAT(false, 4, true),
    DOG(false, 4, true),
    BIRD(true, 2, false),
    ROBOCAT(true, 100, false),
    UNKNOWN(false, 0, false),
    FISH(false, 0, false);

    private final boolean canFly;
    private final int numberOfLegs;
    private final boolean hasFur;

    AnimalSpecies(boolean canFly, int numberOfLegs, boolean hasFur) {
        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
    }

    public boolean canFly() {
        return canFly;
    }

    public int numberOfLegs() {
        return numberOfLegs;
    }

    public boolean hasFur() {
        return hasFur;
    }
}
