package homework09;

import homework09.enums.AnimalSpecies;
import homework09.interfaces.Interfaces.Foulable;

import java.util.HashSet;

public class DomesticCat extends Pat implements Foulable {

    public DomesticCat(AnimalSpecies species, String nickname, Integer age, Integer trickLevel, HashSet<String> habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(species);
    }

    @Override
    public String respond() {
        return "Meow";
    }

    @Override
    public String foul() {
        return "puuur and pee everywhere";
    }

}
