package homework09;

import homework09.enums.AnimalSpecies;
import homework09.interfaces.Interfaces.Foulable;

import java.util.HashSet;

public class RoboCat extends Pat implements Foulable {

    public RoboCat(AnimalSpecies species, String nickname, Integer age, Integer trickLevel, HashSet<String> habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(species);
    }

    @Override
    public String respond() {
        return "I can respond as you want Im a robot";
    }

    @Override
    public String foul() {
        return "I cant foul";
    }

}
