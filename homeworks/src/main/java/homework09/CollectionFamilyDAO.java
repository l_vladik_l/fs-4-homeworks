package homework09;

import homework09.interfaces.FamilyDao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class CollectionFamilyDAO implements FamilyDao {

    private List<Family> families = new ArrayList<>();
    @Override
    public List<Family> getAllFamilies() {
        return families;
    }
    @Override
    public Family getFamilyByIndex(int index) {
        if (index < 0 || index >= families.size()) {
            return null;
        }
        return families.get(index);
    }
    @Override
    public Family getFamilyById(int id) {
        return families.stream().filter(f -> f.getId() == id).findFirst().orElse(null);
    }
    @Override
    public void deleteFamilyByIndex(int index) {
        if (index < 0 || index >= families.size()) {
            return;
        }
        families.remove(index);
    }
    @Override
    public boolean deleteFamily(Family family) {
        families.remove(family);
        return true;
    }
    @Override
    public void saveFamily(Family family) {
        if (families.contains(family)) {
            families.set(families.indexOf(family), family);
        } else {
            families.add(family);
        }

    }
    @Override
    public String displayAllFamilies() {

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < families.size(); i++) {
            Family family = families.get(i);
            sb.append(i).append("\n");
            sb.append(family.toString()).append("\n");
        }
        return sb.toString();
    }
    @Override
    public String getFamiliesBiggerThan(int count) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < families.size(); i++) {
            Family family = families.get(i);
            if (count > family.countFamily()) {
                sb.append(i).append("\n");
                sb.append(family.toString()).append("\n");
            }
        }
        return sb.toString();
    }
    @Override
    public String getFamiliesLessThan(int count) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < families.size(); i++) {
            Family family = families.get(i);
            if (count < family.countFamily()) {
                sb.append(i).append("\n");
                sb.append(family.toString()).append("\n");
            }
        }
        return sb.toString();
    }
    @Override
    public int countFamiliesWithMemberNumber(int count) {
        int familySize = 0;
        for (Family family : families) {
            if (count == family.countFamily()) {
                familySize++;
            }
        }
        return familySize;
    }
    @Override
    public void createNewFamily(int id, Human mother, Human father) {
        Family family = new Family(id, mother, father) {
        };
        families.add(family);

    }
    @Override
    public void deleteAllChildrenOlderThen(int age) {
        families.stream().filter(family -> family.getChildren().stream().anyMatch(child -> child.getYear() > age)).forEach(family -> family.getChildren().removeIf(child -> child.getYear() > age));
    }
    @Override
    public Family adoptChild(Family family, Human child) {
        Family f1 = families.stream()
                .filter(f -> f.equals(family))
                .findFirst()
                .orElse(null);
        if (f1 != null) {
            f1.addChild(child);
        }
        return f1;

    }
    @Override
    public int count() {
        return families.size();
    }
    @Override
    public HashSet<Pat> getPets(int index) {
        if (index < 0 || index >= families.size()) {
            return null;
        }
        Family f = families.get(index);
        return f.getPats();
    }
    @Override
    public void addPet(int index, Pat pat) {
        if (index < 0 || index >= families.size()) {
            return;
        }
        Family f = families.get(index);
        f.addPat(pat);

    }

}
