package homework09.interfaces;

public interface Identifable {
    int id(int uuid);
}
