package homework09;

public final class Man extends Human {

    public Man(String name, String surname, Integer iqLevel, Integer year) {
        super(name, surname, iqLevel, year);
    }
    @Override
    public void greetPat() {
        System.out.println("hello y little boy");
    }
    public void repairCar() {
        System.out.println("I can repair car");
    }
}
