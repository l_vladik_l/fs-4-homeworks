package homework10;

import homework10.Human;

public final class Man extends Human {

    public Man(String name, String surname, Integer iqLevel, Integer year) {
        super(name, surname, iqLevel, year);
    }
    @Override
    public void greetPat() {
        System.out.println("hello y little boy");
    }
    public void repairCar() {
        System.out.println("I can repair car");
    }
}
