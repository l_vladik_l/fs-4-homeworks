package homework10;

import homework10.interfaces.FamilyDao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CollectionFamilyDAO implements FamilyDao {

    private List<Family> families = new ArrayList<>();
    @Override
    public List<Family> getAllFamilies() {
        return families;
    }
    @Override
    public Family getFamilyByIndex(int index) {
        if (index < 0 || index >= families.size()) {
            return null;
        }
        return families.get(index);
    }
    @Override
    public Family getFamilyById(int id) {
        return families.stream().filter(f -> f.getId() == id).findFirst().orElse(null);
    }
    @Override
    public void deleteFamilyByIndex(int index) {
        if (index < 0 || index >= families.size()) {
            return;
        }
        families.remove(index);
    }
    @Override
    public boolean deleteFamily(Family family) {
        families.remove(family);
        return true;
    }
    @Override
    public void saveFamily(Family family) {
        if (families.contains(family)) {
            families.set(families.indexOf(family), family);
        } else {
            families.add(family);
        }

    }
    @Override
    public String displayAllFamilies() {
        return IntStream.range(0, families.size()).mapToObj(x -> x + "\n" + families.get(x) + "\n").collect(Collectors.joining());

    }
    @Override
    public String getFamiliesBiggerThan(int count) {
        return IntStream.range(0, families.size())
                .filter(i -> count > families.get(i).countFamily())
                .mapToObj(i -> i + "\n" + families.get(i).toString() + "\n")
                .collect(Collectors.joining());
    }
    @Override
    public String getFamiliesLessThan(int count) {
        return IntStream.range(0, families.size())
                .filter(i -> count < families.get(i).countFamily())
                .mapToObj(i -> i + "\n" + families.get(i).toString() + "\n")
                .collect(Collectors.joining());
    }
    @Override
    public int countFamiliesWithMemberNumber(int count) {
        return (int) families.stream().filter(x -> count == x.countFamily()).count();
    }
    @Override
    public void createNewFamily(int id, Human mother, Human father) {
        Family family = new Family(id, mother, father) {
        };
        families.add(family);

    }
    @Override
    public void deleteAllChildrenOlderThen(int age) {
        families.stream().filter(family -> family.getChildren().stream().anyMatch(child -> child.getYear() > age)).forEach(family -> family.getChildren().removeIf(child -> child.getYear() > age));
    }
    @Override
    public Family adoptChild(Family family, Human child) {
        Family f1 = families.stream()
                .filter(f -> f.equals(family))
                .findFirst()
                .orElse(null);
        if (f1 != null) {
            f1.addChild(child);
        }
        return f1;

    }
    @Override
    public int count() {
        return families.size();
    }
    @Override
    public HashSet<Pat> getPets(int index) {
        if (index < 0 || index >= families.size()) {
            return null;
        }
        Family f = families.get(index);
        return f.getPats();
    }
    @Override
    public void addPet(int index, Pat pat) {
        if (index < 0 || index >= families.size()) {
            return;
        }
        Family f = families.get(index);
        f.addPat(pat);

    }
}
