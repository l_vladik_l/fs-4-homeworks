package homework10;

import homework10.Human;
import homework10.Man;
import homework10.Pat;
import homework10.Woman;
import homework10.interfaces.Interfaces.HumanCreator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public abstract class Family implements HumanCreator {

    private final homework10.Human mother;
    private final homework10.Human father;
    private final HashSet<homework10.Pat> pat;
    private final int id;

    private final List<homework10.Human> children;

    public Family(int id, homework10.Human mother, homework10.Human father, HashSet<homework10.Pat> pat, ArrayList<homework10.Human> children) {
        this.mother = mother;
        this.father = father;
        this.pat = pat;
        this.children = children;
        this.id = id;
    }

    public Family(int id, homework10.Human mother, homework10.Human father, HashSet<homework10.Pat> pat) {
        this.mother = mother;
        this.father = father;
        this.pat = pat;
        this.children = new ArrayList<>();
        this.id = id;
    }
    public Family(int id, homework10.Human mother, homework10.Human father) {
        this.mother = mother;
        this.father = father;
        this.pat = new HashSet<>();
        this.children = new ArrayList<>();
        this.id = id;
    }

    void addChild(homework10.Human child) {
        this.children.add(child);
    }
    void addPat(homework10.Pat pat) {
        this.pat.add(pat);
    }

    int countFamily() {
        return this.children.size() + 2;
    }
    void removeChild(int idx) {
        if (this.children.isEmpty()) throw new IllegalStateException("no children provided");
        if (idx < 0) throw new IllegalStateException("index must be positive");
        if (idx >= this.children.size()) throw new IllegalStateException("index must be less than length");
        this.children.remove(idx);
    }

    List<homework10.Human> getChildren() {
        return this.children;
    }
    HashSet<homework10.Pat> getPats() {
        return this.pat;
    }
    int getId() {
        return this.id;
    }

    @Override
    public String toString() {
        boolean isChildrenExist = !this.children.isEmpty();
        List<String> childrenNames = this.children.stream().map(homework10.Human::getName).collect(Collectors.toList());
        List<String> patNames = this.pat.stream().map(Pat::getName).collect(Collectors.toList());

        return String.format("motherName=%s, motherSurname=%s, motherYear=%d, motherIq=%d,fatherName=%s, fatherSurname=%s, fatherYear=%d, fatherIq=%d, patNames=%s, children=%s\n",
                this.mother.getName(), this.mother.getSurname(), this.mother.getYear(), this.mother.getIqLevel(),
                this.father.getName(), this.father.getSurname(), this.father.getYear(), this.father.getIqLevel(),
                patNames, isChildrenExist ? childrenNames : new ArrayList<>());
    }

    @Override
    public homework10.Human bornChild(String name, Integer year) {
        homework10.Man man = new Man(name, this.father.getSurname(), (this.father.getIqLevel() + this.mother.getIqLevel()) / 2, year);
        homework10.Woman woman = new Woman(name, this.father.getSurname(), (this.father.getIqLevel() + this.mother.getIqLevel()) / 2, year);
        ArrayList<Human> list = new ArrayList<>();
        list.add(man);
        list.add(woman);
        return list.get(new Random().nextInt(list.size()));
    }

}
