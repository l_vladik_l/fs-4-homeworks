package homework10;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class Human {

    private final String name;
    private final String surname;
    private final Integer iqLevel;
    private final long birthDate;

    public Human(String name, String surname, Integer iqLevel, long birthDate) {
        this.name = name;
        this.surname = surname;
        this.iqLevel = iqLevel;
        this.birthDate = birthDate;

    }

    static Human of(String name, String surname, Integer iqLevel, long birthDate) {
        return new Human(name, surname, iqLevel, birthDate);
    }
    static Human baseHumanInfo(String name, String surname, long birthDate) {
        return new Human(name, surname, null, birthDate);
    }

    public String describeAge() {
        StringBuilder sb = new StringBuilder();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(this.getBirthDate()), ZoneId.systemDefault());
        int year = localDateTime.getYear();
        int month = localDateTime.getMonthValue();
        int dayOfMonth = localDateTime.getDayOfMonth();
        int dayOfWeek = localDateTime.getDayOfWeek().getValue();
        sb.append(String.format("year %d", year));
        sb.append(String.format("month %d", month));
        sb.append(String.format("dayOfMonth %d", dayOfMonth));
        sb.append(String.format("dayOfWeek %d", dayOfWeek));
        return sb.toString();

    }

    public void greetPat() {
        System.out.println("hello y little boy");
    }

    public String getName() {
        return this.name;
    }
    public String getSurname() {
        return this.surname;
    }
    public Integer getYear() {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(this.getBirthDate()), ZoneId.systemDefault()).getYear();
    }
    public Integer getIqLevel() {
        return this.iqLevel;
    }
    public long getBirthDate() {
        return this.birthDate;
    }

    @Override
    public String toString() {
        return String.format("name=%s, surname=%s, year=%d, iq=%d", this.name, this.surname, this.getYear(), this.iqLevel);
    }

}
