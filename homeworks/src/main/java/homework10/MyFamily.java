package homework10;

import homework10.controllers.FamilyController;
import homework10.enums.AnimalSpecies;
import homework10.services.FamilyService;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.WeekFields;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

public class MyFamily {
    public static void main(String[] args) {
        Woman mother = new Woman("motherName", "motherSurname", 100, 1979);
        Man father = new Man("fatherName", "fatherSurnmae", 100, 1963);

        HashSet<String> habits = new HashSet<>();
        habits.add("swim");
        Pat fish = new Fish(AnimalSpecies.FISH, "Bulka", 100, 0, habits);
        Pat roboCat = new RoboCat(AnimalSpecies.ROBOCAT, "ROBOCAT", 100, 0, habits);
        Human baby = homework10.Human.baseHumanInfo("babyName", "babySurname", 2021);
        Human baby2 = Human.baseHumanInfo("baby2Name", "baby2Surname", 2022);

        HashSet<Pat> pats = new HashSet<>();
        pats.add(roboCat);
        pats.add(fish);

        homework10.Family family = new homework10.Family(1, mother, father) {

        };
//        Human bornBaby = family.bornChild("baby", 2022);
//        family.addChild(bornBaby);
//        family.addChild(baby);
//        family.addChild(baby2);
//        family.addChild(baby2);
//        family.removeChild(0);
//        family.removeChild(1);
        AnimalSpecies bird = AnimalSpecies.BIRD;

        FamilyController familyController = new FamilyController(new FamilyService());
        familyController.saveFamily(family);
        familyController.createNewFamily(1, mother, father);

        familyController.addPet(0, fish);
        homework10.Family family1 = familyController.adoptChild(family, baby);
        List<Family> allFamilies = familyController.getAllFamilies();
        int i = familyController.countFamiliesWithMemberNumber(2);
        familyController.deleteAllChildrenOlderThen(1);

        long unixMillisTimestamp = System.currentTimeMillis();
        System.out.println(unixMillisTimestamp);
        LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(unixMillisTimestamp), ZoneId.systemDefault());
        int year = localDateTime.getYear();
        int month = localDateTime.getMonthValue();
        int dayOfMonth = localDateTime.getDayOfMonth();
        int dayOfWeek = localDateTime.getDayOfWeek().getValue();
        WeekFields weekFields = WeekFields.of(Locale.getDefault());
        int weekOfYear = localDateTime.get(weekFields.weekOfYear());

        System.out.println("Year: " + year);
        System.out.println("Month: " + month);
        System.out.println("Day of Month: " + dayOfMonth);
        System.out.println("Day of Week: " + dayOfWeek);
        System.out.println("Week of Year: " + weekOfYear);

    }
}
