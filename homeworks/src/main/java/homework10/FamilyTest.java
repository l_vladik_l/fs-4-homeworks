package homework10;

import homework10.Family;
import homework10.Fish;
import homework10.Human;
import homework10.Man;
import homework10.Pat;
import homework10.Woman;
import homework10.enums.AnimalSpecies;
import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FamilyTest {

    @Test
    public void testAddChild() {
        homework10.Woman mother = new homework10.Woman("motherName", "motherSurname", 100, 1979);
        homework10.Man father = new homework10.Man("fatherName", "fatherSurnmae", 100, 1963);
        HashSet<String> habits = new HashSet<>();
        habits.add("swim");
        homework10.Pat fish = new homework10.Fish(AnimalSpecies.FISH, "Bulka", 100, 0, habits);
        HashSet<homework10.Pat> pats = new HashSet<>();
        pats.add(fish);
        homework10.Family family = new homework10.Family(1, mother, father, pats) {
        };
        homework10.Human baby = homework10.Human.baseHumanInfo("babyName", "babySurname", 2021);
        family.addChild(baby);
        assertEquals(3, family.countFamily());
    }
    @Test
    public void testDeleteChild() {
        homework10.Woman mother = new homework10.Woman("motherName", "motherSurname", 100, 1979);
        homework10.Man father = new Man("fatherName", "fatherSurnmae", 100, 1963);
        HashSet<String> habits = new HashSet<>();
        habits.add("swim");
        homework10.Pat fish = new Fish(AnimalSpecies.FISH, "Bulka", 100, 0, habits);
        HashSet<Pat> pats = new HashSet<>();
        pats.add(fish);
        homework10.Family family = new Family(1, mother, father, pats) {
        };

        homework10.Human baby = homework10.Human.baseHumanInfo("babyName", "babySurname", 2021);
        homework10.Human baby2 = Human.baseHumanInfo("babyName", "babySurname", 2021);
        family.addChild(baby);
        family.addChild(baby2);
        assertEquals(4, family.countFamily());

        family.removeChild(0);

        assertEquals(3, family.countFamily());
    }
    @Test
    public void testToString() {
        homework10.Woman mother = new Woman("motherName", "motherSurname", 100, 1979);
        assertEquals(true, mother.toString() instanceof String);
    }
}
