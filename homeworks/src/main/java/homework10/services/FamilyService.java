package homework10.services;

import homework10.CollectionFamilyDAO;
import homework10.Family;
import homework10.Human;
import homework10.Pat;
import homework10.interfaces.FamilyDao;

import java.util.HashSet;
import java.util.List;

public class FamilyService implements FamilyDao {
    private FamilyDao families = new CollectionFamilyDAO();

    @Override
    public List<Family> getAllFamilies() {
        return families.getAllFamilies();
    }
    @Override
    public Family getFamilyByIndex(int index) {
        return families.getFamilyByIndex(index);
    }
    @Override
    public Family getFamilyById(int id) {
        return families.getFamilyById(id);
    }
    @Override
    public void deleteFamilyByIndex(int index) {
        families.deleteFamilyByIndex(index);
    }

    @Override
    public boolean deleteFamily(Family family) {
        return families.deleteFamily(family);

    }
    @Override
    public void saveFamily(Family family) {
        families.saveFamily(family);
    }
    @Override
    public String displayAllFamilies() {
        return families.displayAllFamilies();
    }
    @Override
    public String getFamiliesBiggerThan(int count) {
        return families.getFamiliesBiggerThan(count);
    }
    @Override
    public String getFamiliesLessThan(int count) {
        return families.getFamiliesLessThan(count);

    }
    @Override
    public int countFamiliesWithMemberNumber(int count) {
        return families.countFamiliesWithMemberNumber(count);
    }
    @Override
    public void createNewFamily(int id, Human mother, Human family) {
        families.createNewFamily(id, mother, family);
    }
    @Override
    public Family adoptChild(Family family, Human child) {
        return families.adoptChild(family, child);
    }
    @Override
    public void deleteAllChildrenOlderThen(int age) {
        families.deleteAllChildrenOlderThen(age);
    }
    @Override
    public int count() {
        return families.count();
    }
    @Override
    public HashSet<Pat> getPets(int index) {
        return families.getPets(index);
    }
    @Override
    public void addPet(int index, Pat pat) {
        families.addPet(index, pat);
    }

}
