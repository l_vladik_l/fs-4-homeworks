package homework10.interfaces;

public interface Identifable {
    int id(int uuid);
}
