package homework10.interfaces;

import homework10.Family;
import homework10.Human;

import java.util.ArrayList;

public class Interfaces {
    public interface HumanCreator {
        Human bornChild(String name, Integer year);
    }

    public interface Foulable {
        String foul();
    }

    public interface FamilyDataAccess {
        ArrayList<Family> getAllFamilies();

        Family getFamilyByIndex();

        boolean deleteFamily();

    }
}
