package homework10.interfaces;

import homework10.Family;
import homework10.Human;
import homework10.Pat;

import java.util.HashSet;
import java.util.List;

public interface FamilyDao {
    List<Family> getAllFamilies();

    Family getFamilyByIndex(int index);

    Family getFamilyById(int id);

    void deleteFamilyByIndex(int index);

    boolean deleteFamily(Family family);

    void saveFamily(Family family);

    String displayAllFamilies();// refactor to Map

    String getFamiliesBiggerThan(int count);

    String getFamiliesLessThan(int count);

    int countFamiliesWithMemberNumber(int count);

    void createNewFamily(int id, Human mother, Human family);

    //bornChild
    Family adoptChild(Family family, Human child);

    void deleteAllChildrenOlderThen(int age);

    int count();

    HashSet<Pat> getPets(int index);

    void addPet(int index, Pat pat);

}
