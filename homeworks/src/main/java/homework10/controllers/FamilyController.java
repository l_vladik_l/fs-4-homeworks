package homework10.controllers;

import homework10.Family;
import homework10.Human;
import homework10.Pat;
import homework10.services.FamilyService;

import java.util.HashSet;
import java.util.List;

public class FamilyController {

    private final FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public Family getFamilyByIndex(int index) {
        return familyService.getFamilyByIndex(index);
    }

    public Family getFamilyById(int id) {
        return familyService.getFamilyById(id);
    }

    public void deleteFamilyByIndex(int index) {
        familyService.deleteFamilyByIndex(index);
    }

    public boolean deleteFamily(Family family) {
        return familyService.deleteFamily(family);

    }

    public void saveFamily(Family family) {
        familyService.saveFamily(family);
    }

    public String displayAllFamilies() {
        return familyService.displayAllFamilies();
    }

    public String getFamiliesBiggerThan(int count) {
        return familyService.getFamiliesBiggerThan(count);
    }

    public String getFamiliesLessThan(int count) {
        return familyService.getFamiliesLessThan(count);

    }

    public int countFamiliesWithMemberNumber(int count) {
        return familyService.countFamiliesWithMemberNumber(count);
    }

    public void createNewFamily(int id, Human mother, Human family) {
        familyService.createNewFamily(id, mother, family);
    }

    public Family adoptChild(Family family, Human child) {
        return familyService.adoptChild(family, child);
    }

    public void deleteAllChildrenOlderThen(int age) {
        familyService.deleteAllChildrenOlderThen(age);
    }

    public int count() {
        return familyService.count();
    }

    public HashSet<Pat> getPets(int index) {
        return familyService.getPets(index);
    }

    public void addPet(int index, Pat pat) {
        familyService.addPet(index, pat);
    }
}
