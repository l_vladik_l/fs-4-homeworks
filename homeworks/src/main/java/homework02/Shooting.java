package homework02;

import libs.ScannerConstrained;

public class Shooting {
    static int axis = 0;
    static String shoot = "X";
    static String emptyCell = "-";
    static int axisShootValue = 6;

    public static int[][] addItem(int[][] arr, int y, int x) {
        int[][] newArr = new int[arr.length + 1][];
        int[] innerArr = {y, x};

        for (int i = 0; i < arr.length; i++) {
            newArr[i] = arr[i];
        }

        newArr[arr.length] = innerArr;
        return newArr;
    }

    public static boolean checkForExistingAxis(int[][] arr, int y, int x) {
        boolean isEqual = false;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i][0] == y && arr[i][1] == x) {
                isEqual = true;
                break;
            }
        }
        return isEqual;
    }

    public static void print(String line) {
        System.out.printf("%s\n", line);
    }

    public static String draw(int value, int x, int y) {
        if (value == axisShootValue) return shoot;
        if (y == axis && x != axis) return Integer.toString(x);
        if (x == axis) return Integer.toString(y);
        return emptyCell;
    }
    public static int randomInt(int min, int max) {
        int range = max - min + 1;
        double random = Math.random() * range;
        return (int) random + min;
    }
    public static void printBoard(int[][] board) {
        for (int y = 0; y < board.length; y++) {
            for (int x = 0; x < board[y].length; x++) {
                System.out.printf(" %s", draw(board[y][x], x, y));
                System.out.print(" |");
            }
            System.out.println();
        }
    }
    public static void fillBoard(int[][] board, int[][] guessedAxis) {
        if (guessedAxis.length == 0) return;
        for (int i = 0; i < guessedAxis.length; i++) {
            int y = guessedAxis[i][0];
            int x = guessedAxis[i][1];
            board[y][x] = axisShootValue;

        }
    }
    public static void main(String[] args) {
        int[][] board = new int[6][6];
        boolean isPlaying = true;
        int[][] guessedAxis = {};
        int randomY = randomInt(1, 5);
        int randomX = randomInt(1, 5);
        System.out.printf("%d", randomY);
        System.out.printf("%d", randomX);

        fillBoard(board, guessedAxis);
        printBoard(board);
        ScannerConstrained scanner = new ScannerConstrained();
        print("All set. Get ready to rumble!");
        while (isPlaying) {
            print("Tell y coordinate!");
            String numberY = scanner.nextLine();
            print("Tell x coordinate!");
            String numberX = scanner.nextLine();

            try {
                int y = Integer.parseInt(numberY);
                int x = Integer.parseInt(numberX);
                if (y >= 6 || x >= 6 || y <= 0 || x <= 0) {
                    print("use coordinates coordinates in range from 1 to 5");
                    continue;
                }
                boolean isEmpty = (guessedAxis.length == 0);
                if (!isEmpty) {
                    boolean isEqual = checkForExistingAxis(guessedAxis, y, x);

                    if (isEqual) {
                        print("use another coordinates");
                        continue;
                    }

                }

                guessedAxis = addItem(guessedAxis, y, x);

                if (y == randomY && x == randomX) {
                    isPlaying = false;
                }

                fillBoard(board, guessedAxis);
                printBoard(board);

            } catch (NumberFormatException ex) {
                print("Your message cant be parsed to a number\n");
            }

        }
        print("You have won!");
    }
}
