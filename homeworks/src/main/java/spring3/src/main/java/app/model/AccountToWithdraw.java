package app.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AccountToWithdraw {
    private String number;
    private Double balance;
}
