package app.dto.employer;

import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@AllArgsConstructor
public class EmployerRequest {
    @NotNull(message = "Customer ID must not be null")
    @Positive(message = "Customer ID must be a positive number")
    private Long customer_id;

    @NotNull(message = "Employer ID must not be null")
    @Positive(message = "Employer ID must be a positive number")
    private Long employer_id;
}
