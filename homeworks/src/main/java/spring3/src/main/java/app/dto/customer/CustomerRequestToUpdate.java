package app.dto.customer;

import lombok.Data;

import javax.validation.constraints.*;

@Data
public class CustomerRequestToUpdate {
    @NotNull(message = "name is required")
    @Size(min = 2, message = "The name must contain at least 2 characters")
    private String name;

    @NotNull(message = "email is required")
    @Email(regexp = "^[\\w.-]+@[\\w.-]+\\.[A-Za-z]{2,}$", message = "Incorrect email format")
    private String email;

    @NotNull(message = "tel is required")
    @Pattern(regexp = "^\\+38\\(0\\d{2}\\)\\d{7}$",
            message = "Incorrect phone number format. Should be +38(0XX)XXXXXXX")
    private String tel;

    @NotNull(message = "age is required")
    @Min(value = 18, message = "The user must be at least 18 years old")
    private Integer age;

}
