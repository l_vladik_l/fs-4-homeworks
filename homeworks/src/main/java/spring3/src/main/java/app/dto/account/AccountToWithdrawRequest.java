package app.dto.account;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import java.util.UUID;

@Data
public class AccountToWithdrawRequest {

    @Pattern(regexp = "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}$", message = "Number must be a valid UUID")
    private String number = UUID.randomUUID().toString();

    @NotNull(message = "Balance must not be null")
    @Positive(message = "Balance must be greater than 0")
    private Double balance;
}
