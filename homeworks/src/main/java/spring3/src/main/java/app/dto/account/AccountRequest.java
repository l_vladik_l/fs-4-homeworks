package app.dto.account;

import app.entity.Currency;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

@Data
@AllArgsConstructor
public class AccountRequest {
    @NotNull(message = "Currency must not be null")
    private Currency currency;
    @PositiveOrZero
    private Double balance;
}
