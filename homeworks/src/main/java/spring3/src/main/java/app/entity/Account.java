package app.entity;

import app.model.AbstractEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "accounts")
public class Account extends AbstractEntity  {
    private String number = UUID.randomUUID().toString();
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Currency currency;
    private Double balance;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    @JsonBackReference
    private Customer customer;

    public Account(Currency currency, Double balance, Customer customer) {
        this.currency = currency;
        this.balance = balance;
        this.customer = customer;
    }

    @Override
    public String toString() {
        return String.format("Account{id=%d, number=%s, currency=%s, balance=%f, customer_id=%d}",
                getId(), number, currency, balance, customer.getId());
    }
}
