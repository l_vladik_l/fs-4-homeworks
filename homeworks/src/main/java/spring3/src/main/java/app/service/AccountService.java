package app.service;

import app.dao.AccountDAO;
import app.entity.Account;
import app.repo.AccountRepo;
import app.repo.CustomerRepo;
import app.utils.Ex;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class AccountService implements AccountDAO<Account> {

    private final AccountRepo accountRepo;
    private final CustomerRepo customerRepo;
    @Override
    public Account save(Account account) {
        return accountRepo.save(account);
    }

    public Account saveAccountForCustomer(Account account, Long customerId) {
        account.setCustomer(customerRepo.
                findById(customerId)
                .orElseThrow(() -> new IllegalArgumentException("Customer not found")));
        return accountRepo.save(account);

    }
    @Override
    public boolean delete(Account obj) {
        return false;
    }
    @Override
    public void deleteAll(List<Account> entities) {
        throw Ex.NI;
    }
    @Override
    public void saveAll(List<Account> entities) {
        throw Ex.NI;
    }
    @Override
    public List<Account> findAll() {
        throw Ex.NI;
    }

    @Override
    public boolean deleteById(long id) {
        try {
            Optional<Account> oAccount = accountRepo.findById(id);
            if (oAccount.isPresent()) {
                accountRepo.delete(oAccount.get());
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public Optional<Account> getOne(long id) {
        return accountRepo.findById(id);
    }

    public Optional<Account> findByAccountNumber(String number) {
        return accountRepo.findByNumber(number);
    }
    public Account accountToReplenish(Account account, Double balance) {
        account.setBalance(account.getBalance() + balance);
        return account;
    }
    public boolean accountToWithdraw(Account account, Double balance) {
        Double currentBalance = account.getBalance();

        if (currentBalance >= balance) {
            account.setBalance(currentBalance - balance);
            return true;
        } else {
            return false;
        }

    }

    public boolean transferFromTo(Account from, Account to, Double balance) {
        Double fromBalance = from.getBalance();

        if (fromBalance >= balance) {
            from.setBalance(fromBalance - balance);
            to.setBalance(to.getBalance() + balance);
            this.save(from);
            this.save(to);
            return true;
        } else {
            return false;
        }
    }

}
