package app.exception;

public class CreateException extends RuntimeException{
    private final String code;

    public CreateException(String message, String code) {
        super(message);
        this.code = code;
    }

    public String getCode(){
        return code;
    }
}
