package app.mapper;

import app.dto.account.AccountRequest;
import app.dto.account.AccountResponse;
import app.entity.Account;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;



@Component
@RequiredArgsConstructor
public class AccountMapper {
    private final ModelMapper modelMapper;

    public AccountResponse convertToAccountResponse(Account account){
        return modelMapper.map(account, AccountResponse.class);
    }
    public Account convertAccountRequestToAccount(AccountRequest accountRequest){
        return modelMapper.map(accountRequest, Account.class);
    }

}
