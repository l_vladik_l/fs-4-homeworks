package app.dto.customer;

import app.entity.Account;
import app.entity.Employer;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class CustomerResponse{
    private Long id;
    private LocalDateTime created_date;
    private LocalDateTime last_modified_date;
    private String name;
    private String email;
    private String tel;
    private Integer age;
    private List<Account> accounts;
    private List<Employer> employers;
}
