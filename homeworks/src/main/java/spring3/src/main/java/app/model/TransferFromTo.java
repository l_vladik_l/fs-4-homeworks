package app.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TransferFromTo {
    private String from;
    private String to;
    private Double balance;
}
