package app.controllers;

import app.dto.account.AccountRequest;
import app.dto.account.AccountResponse;
import app.dto.customer.CustomerRequest;
import app.dto.customer.CustomerRequestToUpdate;
import app.dto.customer.CustomerResponse;
import app.dto.customer.CustomerWorkPlaceRequest;
import app.entity.Account;
import app.entity.Customer;
import app.exception.CreateException;
import app.mapper.AccountMapper;
import app.mapper.CustomerMapper;
import app.model.AbstractEntity;
import app.model.WorkPlaceForCustomer;
import app.service.AccountService;
import app.service.CustomerService;
import app.service.EmployerService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping("customers")
@AllArgsConstructor
@Log4j2
public class CustomerController {

    private final CustomerService customerService;
    private final AccountService accountService;
    private final EmployerService employerService;
    private final CustomerMapper customerMapper;
    private final AccountMapper accountMapper;


    @GetMapping("generate")
    public ResponseEntity<String> generateCustomers() {
        List.of(
                new Customer("Vlad", "vladikvladvita@gmail.com", 24, "DEC47858-D030-4EBE-89F5-0413D7EB16BD"),
                new Customer("Vitalina", "poradavita@gmail.com", 12,"AC7D7EDF-D314-4E4E-9500-BA3524A76DC1"),
                new Customer("Ruslan", "ystymenko@gmail.com", 45, "634FE01C-1D65-49FB-9BEB-A1928C744F75"),
                new Customer("Nasty", "komarova@gmail.com", 37, "38EC85B6-7C04-4A38-9F6C-16D895E70B2D")
        ).forEach(customerService::save);
        return ResponseEntity.ok().body("customer have been generated");
    }

    @PostMapping("create")
    public ResponseEntity<CustomerResponse> createCustomer(@Valid @RequestBody CustomerRequest customerRequest) {
        return Optional.of(customerMapper.convertRequestToCustomer(customerRequest))
                .map(customerService::save)
                .map(customerMapper::convertCustomerToResponse)
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new CreateException("Failed to create customer", "CUSTOMER_CREATION_FAILED"));
    }

    @GetMapping("{id}")
    public ResponseEntity<CustomerResponse> getOne(@PathVariable("id") Long id) {
        return customerService.getOne(id)
                .map(customerMapper::convertCustomerToResponse)
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new EntityNotFoundException("Customer was not found with id: " + id));

    }
    @GetMapping("all")
    public ResponseEntity<List<CustomerResponse>> getAll() {
        List<Customer> allCustomers = customerService.findAll();
        return ResponseEntity.ok(allCustomers.stream()
                .map(customerMapper::convertCustomerToResponse)
                .collect(toList()));
    }

    @PutMapping("{id}")
    public ResponseEntity<?> updateCustomer(@PathVariable Long id, @Valid @RequestBody CustomerRequestToUpdate customerRequestToUpdate) {
        Optional<Customer> found = customerService.getOne(id);

        if (found.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        return customerService.getOne(id)
                .map(existingCustomer -> {
                    Customer customer = customerMapper.convertRequestToUpdateToCustomer(customerRequestToUpdate);
                    customer.setId(existingCustomer.getId());
                    Customer updatedCustomer = customerService.save(customer);
                    CustomerResponse customerResponse = customerMapper.convertCustomerToResponse(updatedCustomer);
                    return ResponseEntity.ok(customerResponse);
                })
                .orElseThrow(() -> new EntityNotFoundException("Customer with ID: " + id));
    }

    @DeleteMapping("/remove/{id}")
    public ResponseEntity<String> removeCustomer(@PathVariable("id") Long id) {
        return Optional.of(customerService.deleteById(id))
                .filter(Boolean::booleanValue)
                .map((delete) -> ResponseEntity.ok(String.format("Customer ID %s deleted successfully", id)))
                .orElseThrow(()-> new EntityNotFoundException("Customer with ID " + id));
    }

    @PostMapping("{id}/account/create")
    public ResponseEntity<?> createAccountForCustomer(@PathVariable Long id, @RequestBody AccountRequest accountRequest) {
        return customerService.getOne(id)
                .map(customer -> {
                    Account account = accountMapper.convertAccountRequestToAccount(accountRequest);
                    Account response = accountService.saveAccountForCustomer(account, id);
                    AccountResponse accountResponse = accountMapper.convertToAccountResponse(response);
                    return ResponseEntity.ok(accountResponse);
                })
                .orElseThrow(() -> new EntityNotFoundException("Customer with ID " + id));
    }

    @Transactional
    @PostMapping("workplace/register")
    public ResponseEntity<?> registerWorkPlaceForCustomer(@Valid @RequestBody CustomerWorkPlaceRequest body) {
        List<? extends AbstractEntity> entities = Stream.of(customerService.getOne(body.getCustomer_id()),
                        employerService.getOne(body.getEmployer_id()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(toList());

        if (entities.size() < 2) {
            return ResponseEntity.badRequest().body("Customer or Employer not found");
        }

        employerService.saveWorkPlaceForCustomer(entities);

        return ResponseEntity.ok("Workplace registered successfully for the customer");
    }
}
