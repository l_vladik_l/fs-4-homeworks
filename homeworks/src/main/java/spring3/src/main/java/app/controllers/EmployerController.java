package app.controllers;

import app.entity.Customer;
import app.entity.Employer;
import app.model.WorkPlaceForCustomer;
import app.service.CustomerService;
import app.service.EmployerService;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("workplace")
@AllArgsConstructor
public class EmployerController {
    private final EmployerService employerService;

    @GetMapping("generate")
    public ResponseEntity<String> generateWorkplaces() {
        List.of(
                new Employer("Luxoft", "Gagarina 10"),
                new Employer("Epam", "Moshina 5"),
                new Employer("SoftServe", "Silinskogo 18"),
                new Employer("SoftConstract", "Bolshogo 28")
        ).forEach(employerService::save);
        return ResponseEntity.ok().body("workplaces have been generated");
    }


}
