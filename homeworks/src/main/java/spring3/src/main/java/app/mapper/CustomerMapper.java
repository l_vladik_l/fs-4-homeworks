package app.mapper;

import app.dto.customer.CustomerRequest;
import app.dto.customer.CustomerRequestToUpdate;
import app.dto.customer.CustomerResponse;
import app.entity.Customer;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;


@Component
@RequiredArgsConstructor
public class CustomerMapper {
    private final ModelMapper modelMapper;

    public Customer convertRequestToCustomer(CustomerRequest customerRequest) {
        return modelMapper.map(customerRequest, Customer.class);
    }
    public Customer convertRequestToUpdateToCustomer(CustomerRequestToUpdate customerRequestToUpdate) {
        return modelMapper.map(customerRequestToUpdate, Customer.class);
    }
    public CustomerResponse convertCustomerToResponse(Customer customer) {
        CustomerResponse resp = modelMapper.map(customer, CustomerResponse.class);
        resp.setCreated_date(customer.getCreatedDate());
        resp.setLast_modified_date(customer.getLastModifiedDate());
        return resp;
    }

}
