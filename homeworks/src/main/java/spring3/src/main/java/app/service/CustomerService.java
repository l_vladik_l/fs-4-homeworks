package app.service;

import app.dao.CustomerDAO;
import app.entity.Customer;
import app.repo.CustomerRepo;
import app.utils.Ex;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class CustomerService implements CustomerDAO<Customer> {

    private final CustomerRepo customerRepo;
    @Override
    public Customer save(Customer c) {
        return customerRepo.save(c);
    }


//    @Override
//    public Customer save(Customer c) {
//        Customer customer = new Customer();
//        customer.setName(c.getName());
//        customer.setEmail(c.getEmail());
//        customer.setAge(c.getAge());
//        if (c.getId() != null) customer.setId(c.getId());
//
//        customerRepo.save(customer);
//        return customer;
//
//    }


    @Override
    public boolean delete(Customer obj) {
        throw Ex.NI;
    }
    @Override
    public void deleteAll(List<Customer> entities) {
        throw Ex.NI;
    }
    @Override
    public void saveAll(List<Customer> entities) {
        throw Ex.NI;
    }
    @Override
    public List<Customer> findAll() {
        return customerRepo.findAll();
    }
    @Override
    public boolean deleteById(long id) {
        try {
            Optional<Customer> oAccount = customerRepo.findById(id);
            if (oAccount.isPresent()) {
                customerRepo.delete(oAccount.get());
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }

    }
    @Override
    public Optional<Customer> getOne(long id) {
        return customerRepo.findById(id);
    }
}
