package app.entity;

import app.model.AbstractEntity;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "workplace")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Employer extends AbstractEntity {
    String name;
    String address;

    @ManyToMany(mappedBy = "employers")
    List<Customer> customers = new ArrayList<>();

    public Employer(String name, String address) {
        this.name = name;
        this.address = address;
    }

    @Override
    public String toString() {
        return "Employer{" +
                "id=" + getId() +
                ", name='" + name + '\'' +
                ", email='" + address + '\'' +
                '}';
    }

}
