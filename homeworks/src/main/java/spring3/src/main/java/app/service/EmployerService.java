package app.service;

import app.entity.Customer;
import app.entity.Employer;
import app.model.AbstractEntity;
import app.repo.CustomerRepo;
import app.repo.EmployerRepo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class EmployerService {
    private final EmployerRepo employerRepo;


    public Employer save(Employer e) {
        return employerRepo.save(e);
    }

    public void saveWorkPlaceForCustomer(List<? extends AbstractEntity> entities) {
        Customer customer = (Customer) entities.stream().filter(item -> item instanceof Customer).findFirst().orElse(null);
        Employer employer = (Employer) entities.stream().filter(item -> item instanceof Employer).findFirst().orElse(null);
        customer.getEmployers().add(employer);
        employer.getCustomers().add(customer);

        employerRepo.save(employer);
    }

    public Optional<Employer> getOne(Long id) {
        return employerRepo.findById(id);
    }

}
