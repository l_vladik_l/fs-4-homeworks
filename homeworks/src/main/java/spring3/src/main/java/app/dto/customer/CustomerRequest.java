package app.dto.customer;

import lombok.Data;

import javax.validation.constraints.*;

@Data
public class CustomerRequest {
    @Size(min = 2, message = "The name must contain at least 2 characters")
    private String name;

    @NotNull
    @Email(regexp = "^[\\w.-]+@[\\w.-]+\\.[A-Za-z]{2,}$", message = "Incorrect email format")
    private String email;

    @Pattern(regexp = "^\\+38\\(0\\d{2}\\)\\d{7}$",
            message = "Incorrect phone number format. Should be +38(0XX)XXXXXXX")
    private String tel;

    @Min(value = 18, message = "The user must be at least 18 years old")
    private Integer age;
    @NotNull(message = "password is required")
    private String password;
}
