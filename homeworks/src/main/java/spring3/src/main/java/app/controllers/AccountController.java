package app.controllers;

import app.dto.account.AccountResponse;
import app.entity.Account;
import app.entity.Currency;
import app.entity.Customer;
import app.mapper.AccountMapper;
import app.model.AccountToReplenish;
import app.model.AccountToWithdraw;
import app.model.TransferFromTo;
import app.service.AccountService;
import app.service.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

@RestController
@RequestMapping("accounts")
@AllArgsConstructor
public class AccountController {
    private final AccountService accountService;
    private final CustomerService customerService;
    private final AccountMapper accountMapper;

    @GetMapping("generate")
    public void generateAccounts() {
        List<Long> customersId = customerService.
                findAll()
                .stream()
                .map(Customer::getId).toList();

        Optional
                .of(customersId)
                .filter(x -> !x.isEmpty())
                .orElseThrow(() -> new RuntimeException("Customer should be generated first"));

        Currency[] currency = Currency.values();

        customersId
                .forEach(id -> IntStream.range(0, 4)
                        .forEach(i -> accountService.saveAccountForCustomer(
                                new Account(currency[i], (double) ThreadLocalRandom.current().nextInt(1, 10000), new Customer()), id))

                );

    }


    @DeleteMapping("delete/{id}")
    public ResponseEntity<String> deleteAccount(@PathVariable("id") Long id) {
        return Optional.of(accountService.deleteById(id))
                .filter(Boolean::booleanValue)
                .map((delete) -> ResponseEntity.ok("Account was deleted"))
                .orElseThrow(() -> new EntityNotFoundException("Account with ID " + id));
    }

//    @GetMapping("{id}")
//    public ResponseEntity<CustomerResponse> getOne(@PathVariable("id") Long id) {
//        return customerService.getOne(id)
//                .map(customerMapper::convertCustomerToResponse)
//                .map(ResponseEntity::ok)
//                .orElseThrow(() -> new EntityNotFoundException("Customer was not found with id: " + id));
//
//    }

    @GetMapping(value = "{id}", produces = "application/json")
    public ResponseEntity<AccountResponse> getOne(@PathVariable("id") Long id) {
        return accountService.getOne(id)
                .map(accountMapper::convertToAccountResponse)
                .map(x -> {
                    System.out.println("------------------------------");
                    System.out.println(x);
                    System.out.println("------------------------------");
                    return ResponseEntity.ok().body(x);
                })
                .orElseThrow(() -> new EntityNotFoundException("Account with id: " + id));
    }

    @PutMapping("top")
    public ResponseEntity<String> replenishAccount(@RequestBody AccountToReplenish ator) {
        return accountService.findByAccountNumber(ator.getNumber()).
                map(a -> {
                    accountService.accountToReplenish(a, ator.getBalance());
                    accountService.save(a);
                    return ResponseEntity.ok("Account have been replenished");
                })
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).body("Account not found"));
    }
    @PutMapping("withdraw")
    public ResponseEntity<String> replenishAccount(@RequestBody AccountToWithdraw atow) {
        return accountService.findByAccountNumber(atow.getNumber()).
                map(a -> accountService.accountToWithdraw(a, atow.getBalance()) ? ResponseEntity.ok("Account have been withdraw") : ResponseEntity.ok("Not enough money on balance"))
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).body("Account not found"));
    }

    @PutMapping("transfer")
    public ResponseEntity<String> replenishAccount(@RequestBody TransferFromTo fromTo) {
        Optional<Account> from = accountService.findByAccountNumber(fromTo.getFrom());
        Optional<Account> to = accountService.findByAccountNumber(fromTo.getTo());

        return from
                .flatMap(f -> to
                        .map(t -> accountService.transferFromTo(f, t, fromTo.getBalance()) ?
                                ResponseEntity.ok("Transfer was successful") :
                                ResponseEntity.ok("Not enough balance")))
                .orElseGet(() -> ResponseEntity
                        .status(HttpStatus.NOT_FOUND)
                        .body("'from' or 'to' account not found"));
    }
}
