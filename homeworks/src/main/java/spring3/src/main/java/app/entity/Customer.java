package app.entity;

import app.model.AbstractEntity;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "customers")
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class Customer extends AbstractEntity  {
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String email;
    private String tel;
    private Integer age;
    @Column(nullable = false)
    private String password;

    @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Account> accounts;

    @ManyToMany(fetch = FetchType.EAGER)
    private List<Employer> employers = new ArrayList<>();

    public Customer(String name, String email, Integer age, String password) {
        this.name = name;
        this.email = email;
        this.age = age;
        this.password = password;
    }

    @Override
    public String toString() {
        return String.format("Customer{id=%d, Name=%s, Email=%s, Age=%d, Accounts=%s, Employers=%s}",
                getId(), name, email, age, accounts, employers);
    }
}
