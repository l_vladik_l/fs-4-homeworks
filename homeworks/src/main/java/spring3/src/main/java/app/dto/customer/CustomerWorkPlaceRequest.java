package app.dto.customer;

import lombok.Data;
import javax.validation.constraints.NotNull;

@Data
public class CustomerWorkPlaceRequest {
    @NotNull(message = "customer id is required")
    private Long customer_id;
    @NotNull(message = "employer id is required")
    private Long employer_id;
}
