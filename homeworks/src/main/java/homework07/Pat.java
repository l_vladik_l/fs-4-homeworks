package homework07;

import java.util.Arrays;
import java.util.HashSet;

public abstract class Pat {

    private AnimalSpecies species;
    private final String nickname;
    private final Integer age;
    private final Integer trickLevel;
    private final HashSet<String> habits;

    public Pat(String nickname, Integer age, Integer trickLevel, HashSet<String> habits) {

        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pat() {
        this.nickname = "";
        this.age = 0;
        this.trickLevel = 0;
        this.habits = new HashSet<>();
    }
    public Pat(String nickname) {
        this.nickname = nickname;
        this.age = 0;
        this.trickLevel = 0;
        this.habits = new HashSet<>();
    }

    protected void setSpecies(AnimalSpecies species) {
        this.species = species;
    }

    @Override
    public String toString() {
        return String.format("%s {nickname=%s, age=%d, trickLevel=%d, habits=%s", this.species, this.nickname, this.age, this.trickLevel, this.habits);
    }

    public String eat() {
        return "Im eating";
    }
    public abstract String respond();

    public Integer getTrickLevel() {
        return this.trickLevel;
    }

    public String getName() {
        return this.nickname;
    }
    public Integer getPatAge() {
        return this.age;
    }
    public HashSet<String> getHabits() {
        return this.habits;
    }

}
