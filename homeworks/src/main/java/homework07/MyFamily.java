package homework07;

import java.util.HashSet;

public class MyFamily {
    public static void main(String[] args) {
        Woman mother = new Woman("motherName", "motherSurname", 100, 1979);
        Man father = new Man("fatherName", "fatherSurnmae", 100, 1963);
        mother.makeUp();
        father.repairCar();
        HashSet<String> habits = new HashSet<>();
        habits.add("swim");
        Pat fish = new Fish(AnimalSpecies.FISH, "Bulka", 100, 0, habits);
        Pat roboCat = new RoboCat(AnimalSpecies.ROBOCAT, "ROBOCAT", 100, 0, habits);
        Human baby = Human.baseHumanInfo("babyName", "babySurname", 2021);
        Human baby2 = Human.baseHumanInfo("baby2Name", "baby2Surname", 2022);

        HashSet<Pat> pats = new HashSet<>();
        pats.add(roboCat);
        pats.add(fish);

        Family family = new Family(mother, father, pats) {
        };
        Human bornBaby = family.bornChild("baby", 2022);
        family.addChild(bornBaby);
        family.addChild(baby);
        family.addChild(baby2);
        family.addChild(baby2);
        System.out.println(family);
        family.removeChild(0);
        family.removeChild(1);
        AnimalSpecies bird = AnimalSpecies.BIRD;

    }
}
