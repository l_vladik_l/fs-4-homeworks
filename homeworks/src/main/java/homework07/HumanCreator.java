package homework07;

public interface HumanCreator {

    Human bornChild(String name, Integer year);
}
